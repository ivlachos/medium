# Πολιτισμός και Μαραθώνιος Καινοτομίας Crowdhackathon

*Από τον Ιωάννη Βλάχο / Open Innovation Lead της [Crowdpolicy](http://crowdpolicy.com/)*

## Πολιτισμός και Τεχνολογία

Οι νέες τεχνολογίες, όπως οι μηχανική μάθηση (machine learning), η τεχνητή νοημοσύνη (artificial intelligence), οι αλυσίδες συστοιχιών (blockchains) κι άλλες, έχουν ήδη αρχίσει να έχουν καταλυτική επίδραση σε διάφορους τομείς, όπως οι οικονομικές και τραπεζικές υπηρεσίες, η ενέργεια, η υγεία και η διακυβέρνηση. Ο τομέας το του πολιτισμού, αποτελεί ίσως τον τομέα εκείνο ο οποίος έχει επιρεασθεί λιγότερο από τις προαναφερθείσες τεχνολογίες. Για τον λόγο αυτό, οι εξελίξεις στον τομέα του πολιτισμού τα επόμενα χρόνια αναμένονται ραγδαίες υπό την επίδραση **ανατρεπτικών τεχνολογιών (disruptive technologies)**.

## O Μαραθώνιος Καινοτομίας TAP 2 OPEN Bootcamp είναι εδώ!

To [Ταμείο Αρχαιολογικών Πόρων](http://www.tap.gr/tapadb/index.php) με την υποστήριξη της [Crowdpolicy](http://crowdpolicy.com/) διοργανώνει διήμερο διαγωνισμό καινοτομίας **[TAP 2 OPEN Bootcamp](http://www.crowdhackathon.com/tap2open/)** με στόχο την ανάπτυξη καινοτόμων εφαρμογών για την βελτίωση της εμπειρίας των επισκεπτών σε χώρους πολιτισμού.

![](http://www.crowdhackathon.com/tap2open/img/cover.jpg)

Η δράση αποτελεί μέρος της συνολικής στρατηγικής του Ταμείου Αρχαιολογικών Πόρων για τη βελτίωση της εμπειρίας των επισκεπτών στους χώρους πολιτισμού, την υιοθέτηση νέων διεθνών καλών πρακτικών, την προώθηση του ψηφιακού μετασχηματισμού στο χώρο του πολιτισμού και τη δημιουργία εμπειρίας και τεχνογνωσίας, μέσα από διαδικασίες ανοιχτής καινοτομίας, για μια ανθρωποκεντρική προσέγγιση στο χώρο του πολιτισμού με στόχο την ενίσχυση και προώθηση της πολιτιστικής μας κληρονομιάς.

Στο **[TAP 2 OPEN Bootcamp](http://www.crowdhackathon.com/tap2open/)** θα αναπτυχθούν εφαρμογές για την εμπειρία του επισκέπτη κατά την επίσκεψή του στο Αρχαιολογικό Μουσείο Θεσσαλονίκης (visitor experience) καθώς και για την ανάλυση δεδομένων και τάσεων σχετικά με τις επισκέψεις (visitor experience behaviour and data analytics). Στο Bootcamp θα συμμετέχουν μέντορες από τον χώρο του πολιτισμού και της τεχνολογίας για να συνδράμουν τους διαγωνιζόμενους με τεχνογνωσία για τα πεδία (challenges) αλλά και τις αρχές ανάπτυξης προϊόντων και υπηρεσιών μέσα από εργαστήρια (workshops) για ανθρωποκεντρικό σχεδιασμό (design thinking) και ευέλικτη μεθοδολογία ανάπτυξης (agile development).

Στόχοι της διοργάνωσης είναι:

- η ανάπτυξη εφαρμογών για τα challenges visitor experience και visitor experience behaviour and data analytics,
- η υποστήριξη της πολιτιστικής μικρο-επιχειρηματικότητας και της πολιτιστικής τεχνολογίας και
- η καθιέρωση ανατρεπτικών τεχνολογιών (disruptive technologies) σε πολιτιστικά σημεία ενδιαφέροντος (μουσεία, εκθέσεις, αρχαιολογικούς χώρους κλπ)

## Η Τεχνολογία στον Πολιτισμός και ο Πολιτισμός στην Τεχνολογία

Ο τομέας του πολιτισμού, παρά το γεγονός ότι αποτελεί κεντρικό στοιχείο και σημείο αναφοράς κάθε κοινωνίας,

Ποιες είναι λοιπόν εκείνοι οι τομείς και οι εφαρμογές οι οποίες αναμένεται να ανατρέψουν ριζικά τον τρόπο με τον οποίο αλληλεπιδρούμε με την πολιτιστική μας κληρονομιά; Ας δούμε στη συνέχεια μερικούς τέτοιοις τομείς.

### 1. Enhanced User Experience

Η **επαυξημένη πραγματικότητα (augmented reality)** αποτελεί μία τεχνολογία που βρίσκει ποικίλες εφαρμογές στο χώρο του πολιτισμού και συγκεκριμένα σε μουσεία, εκθέσεις και αρχαιλογικούς χώρους. Η πιο απλή και συνηθησμένη εφαρμογή της επαυξημένη πραγματικότητας είναι η προσθήκη πολυμεσικών πληροφοριών (κείμενο, ήχος, εικόνα και βίντεο) σε έργα τέχνης, ώστε να βελτιώνουν τη εμπειρία του χρήστη/επισκέπτη με το να παρέχουν επειπλέον χρήσιμες πολυμεσικές πληροφορίες σχετικά με το έργο το οποίο περιεργάζεται. Επιπλέον, η επαυξημένη πραγματικότητα δίνει τη δυνατότητα να προσθέσουμε μία επιπλέον διάσταση στα διάφορα έργα, "ζωντανεύοντας" με τον τρόπο αυτό τα ίδια τα αντικείμενα. 



*Photo by [Billetto Editorial](https://unsplash.com/@billetto?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/augmented-reality?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)*

Παραθέτουμε στη συνέχεια μερικά παραδείγματα από την εφαρμογή της επαυξημένης πραγματικότητας στον πολιτισμό, που στόχο έχουν να διευρύνουν τη βάση και το πλήθος των ενδιαφερομένων για τον πολιτισμό.

- [National Museum of Singapore](https://youtu.be/OMv92DpcgfI)
- [Art Gallery of Ontario, Toronto](https://youtu.be/mHFzkV20lwQ)
- [The Smithsonian Institution, Washington D.C.](https://youtu.be/7agVb4IG16M)

Επιπλέον, τα **εικονικά μουσεία (virtual museums)** αποτελούνται από πολυμεσικό περιεχόμενο, όπως ψηφιακές ή ψηφιοποιημένες εικόνες, αρχεία ήχου και κειμένου, καθώς και δεδομένα ιστορικού, επιστημονικού ή πολιτισμικού χαρακτήρα. Τα εικονικά μουσεία δεν φιλοξενούν πραγματικά εκθέματα, αλλά μέσω του πολυμεσικού περιεχομένου που διαθέτουν παρέχουν στον χρήστη-επισκέπτη τη δυνατότητα να έχει πρόσβαση σε πολλαπλές αναπαραστάσεις του ίδιου έργου η αντικειμένου, ενώ ταυτόχρονα επιτρέπουν την απομακρυσμένη πρόσβαση στα εικονικά εκθέματα για μελέτη ή απλή θέαση. Αξίζει να αναφέρουμε, ότι μέσω της χρήσης απτικών τεχνολογιών (haptic technologies), ο χρήστης-επισκέπτης έχει, σε ορισμένες περιπτώσεις, τη δυνατότητα να "νιώσει" τις ανάγλυφες επιφάνειες των εκθεμάτων.

Παραθέτουμε στη συνέχεια μερικά παραδείγματα εικονικών μουσείων ανά τον κόσμο.

- [National Museum of Natural History](http://naturalhistory.si.edu/vt3/)
- [Renwick Gallery, Smithsonian American Art Museum](http://americanart.si.edu/multimedia/wonder360/)
- [National Museum of Iraq](https://edu.google.com/expeditions/)
- [The Louvre](http://www.youvisit.com/tour/louvremuseum)
- [The National Museum of Computing](https://my.matterport.com/show/?m=Vz8kCqGRjQA)

Τέλος, αξίζει αν αναφέρουμε τη χρήση ψηφιακών βοηθών (digital assistants) που βασίζονται σε τεχνολογιές τεχνητής νοημοσύνης και επεξεργασία φυσικής γλώσσας (natural language processing) για να προσφέρουν στους χρήστες βελτιωμένες εμπειρίες ξεναγήσεων σε μουσεία με χρήση για παράδειμα chatbots.

### 2. 3D Scanning and Imaging

Η χρήση τεχνολογιών τρισδιάστατης απεικόνισης/σάρωσης και εκτύπωσης, επιτρέπει με μεγάλη ακρίβεια την αναπαράσταση και αναπαραγωγή αντικειμένων. Έτσι, καθίσταται δυνατή όχι μόνο η δημιουργία για παράδειγμα εικόνων υψηλής ανάλυσης από έργα τέχνης, που επιτρέπουν ακόμη και τη μελέτη της τεχνοτροπίας του δημιουργού του έργου. Επιπλέον, δίνουν τη δυνατότητα να δημουργηθούν ψηφιακά αντίτυπα υψηλής πιστότητας διδιάστατων και τρισδιάστατων έργων τέχνης, τα οποία εκτός από τη δυνάτοτητα απομακρισμένης μελέτης του ίδιου του έργου, γίνονται προσβάσιμα με ηλεκτρονικό τρόπο σε όλο και μεγαλύτερο κοινό.

### 3. Cultural Analytics

Ο όρος **cultural analytics** αναφέρεται στην ανάλυση δεδομένων που προέρχονται από μεγάλα δεδομένα (big data) από έργα τέχνης μέσω της ψηφιοποίησής τους με διάφορους τρόπους (3D scanning, high-resolution digital imaging, κλπ.), παρέχοντας έτσι νέες δυνατότηες για την μελέτη των έργων όσο και των δημιουργών τους.

### 4. Non-Distructive Testing

Οι **μη καταστρεπτικές δοκιμές (non-distructive testing)** αποτελούν σήμερα ισχυρό εργαλείο, τόσο για τη λεπτομερή ανάλυση των έργων τέχνης, όσο και για την πιστοποίηση της αυθεντικότητάς τους, καθώς και για τη μελέτη ιστορικών στοιχείων γύρω από το ίδιο το έργο και τον δημιουργό του. Οι μη καταστρεπτικές δοκιμές χρησιμοποιούν μία ευρεία γκάμα από τεχνολογίες, όπως ραδιογραφία (radiography),  χρήση υπερήχων (ultrasound) και άλλες. Καθώς οι μέθοδοι αυτές είναι μη επεμβατικές, προστατεύουν το έργο και παρέχουν πληθώρα χρήσιμων δεδομένων.

### 5. Provenance and certification

Ένας ενδιαφέρον και με μεγάλη σημασία τομέας που σχετίζεται με την πολιτιστική μας κληρονομιά αφορά  στην πιστοποίηση και στην απόδειξη κυριότητας αντικειμένων (provenance and certification). Με δεδομένο ότι ο κόσμος της τέχνης κατακλύζεται από πλαστά ή κλεμμένα έργα και αντικείμενα, προκύπτει όλο και πιο έντονα η ανάγκη ύπαρξης τρόπων και μεθόδων, οι οποίοι άμεσαμ με διαφάνεια  και πέραν από κάθε αμφιβολία θα είναι σε θέση να πιστοποιήσουν την αυθεντικότητα ενός έργου ή αντικειμένου, καθώς και τον νόμιμο κάτοχό του. Για την επίτευξη των παραπάνω στόχων, η **τεχνολογία των αλυσίδων συστοιχιών (blockchain technology)** έχει αρχίσει να χρησιμοποιείται, εκμεταλλευόμενοι τις εγγενείς ιδιότητες των blockchains, όπως η διαφάνεια των συναλλαγών, η έλλειψη δυνατότητας μεταβολής του μητρώου συναλλαγών και η κατανεμημένη εμπιστοσύνη μεταξύ των όλων των συμμετεχόντων μερών. 

Παραθέτουμε στη συνέχεια μερικά παραδείγματα από την εφαρμογή των blockchains στο πεδίο του provenance και certification.

- [**Artoty:**](https://www.artory.com/) The Artory registry tracks provenance for art and other collectibles using a blockchain ledger to make sure that custody chains are authentic and tamper-proof. The registry provides security and anonymity for buyers and collectors. It simultaneously assures prospective buyers that they have all the accurate information they need before making a purchase decision.
- [**Verisart:**](https://verisart.com/) Verisart is a blockchain platform that helps users create secure digital certificates for art and collectibles. Using the blockchain, they can bind these works to detailed provenance records. They aim to create more tamper-proof certificates of authenticity and ownership to reduce fraud and protect legitimate art exchanges.
- **[Blockchain Art Collective:](https://blockchainartcollective.com/)** Blockchain Art Collective is a growing movement of artists using the blockchain to verify a work of art’s origin, authenticity, and journey. They do this by creating certificates of authenticity using blockchain technology, and make that data available to all relevant parties. Their goal is to give art a “digital life,” and they hope to improve the market for artists and art buyers alike.

## Toolkits and Development Frameworks

Στη συνέχεια παραθέτουμε κάποιους συνδέσμους για τεχνολογίες οι οποίες μπορούν να σας βοηθήσουν να σχεδιάσετε και να υλοποιήσετε τις ιδέες σας.

- **Augmented Reality**
  - [ARKit 3:](https://developer.apple.com/augmented-reality/) Apple Augmented Reality Kit
  - [Wikitude:](https://www.wikitude.com/store/) Augmented Reality SDK
- **Chatbots**
  - [Microsoft Bot Framework](https://dev.botframework.com)
  - [wit.ai - Natural language for developers](https://wit.ai)
  - [Dialogflow](https://dialogflow.com)
  - [Botkit](https://botkit.ai)
  - [RASA](https://rasa.com)
- **Virtual Tours and Exhibitions**
  - [Roundme](https://roundme.com/)
  - [Google Tour Creator](https://vr.google.com/tourcreator/)
  - [Artsteps](https://www.artsteps.com)
  - [Matterport](https://matterport.com)
- **Blockchains**
  - [Ethereum](https://www.ethereum.org)
  - [Hyperledger](https://www.hyperledger.org)
  - [Corda](https://www.corda.net)

## TAP 2 OPEN Bootcamp

Σας περιμένουμε στο bootcamp που θα πραγματοποιηθεί στο πλαίσιο της Διεθνούς Έκθεσης Θεσσαλονίκης στις **7** και **8 Σεπτεμβρίου 2019**, στον χώρο του [Αρχαιολογικού Μουσείου Θεσσαλονίκης](https://www.amth.gr/) . Το πρόγραμμα του [**TAP 2 OPEN Bootcamp**](http://www.crowdhackathon.com/tap2open/) έχει ως εξής:

### Σάββατο 7 Σεπτεμβρίου 2019

- 10:00 - 11:00 Registrations - Check in
- 11:00 - 13:00 Kick Off meeting, Παρουσίαση του οράματος και των στόχων της πρωτοβουλίας TAP 2 Open

### Κυριακή 8 Σεπτεμβρίου 2019

- 10:00 - Έναρξη Bootcamp - Διαγωνισμού
- 10:00 - 11:00 Design Thinking Workshop
- 14:00 - 15:00 Agile Development Workshop
- 17:00 - 19:00 Παρουσιάσεις Ομάδων - Pitching
- 19:00 - 20:00 Ανακοίνωση αποτελεσμάτων και απονομή βραβείων - Networking

Περισσότερες πληροφορίες μπορείτε να βρείτε στους ακόλουθους συνδέσμους.

- Site Ενημέρωσης: http://www.crowdhackathon.com/tap2open/
- Facebook Event: https://www.facebook.com/events/414789509157769/
- Εγγραφές: http://ticketing.crowdhackathon.com/crowdpolicy/tap2open-bootcamp

## Χρήσιμοι Σύνδεσμοι

- [How museums are using immersive digital experiences](https://econsultancy.com/how-museums-are-using-immersive-digital-experiences/)
- [12 Startups Using Blockchain to Transform the Art Industry](https://www.disruptordaily.com/blockchain-market-map-art/)
- [Technology in Museums – introducing new ways to see the cultural world](https://advisor.museumsandheritage.com/features/technology-museums-introducing-new-ways-see-cultural-world/)
- [Scan the world](https://www.myminifactory.com/scantheworld/#home)
